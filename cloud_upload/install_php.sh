#!/bin/bash
sudo apt-get -yf install python-software-properties
sudo add-apt-repository -y ppa:ondrej/php
sudo apt-get update
sudo apt-get -yf purge php5-fpm
sudo apt-get -yf install php7.0-cli php7.0-common php7.0 php7.0-mysql php7.0-curl php7.0-gd php7.0-bz2 php7.0-bcmath php7.0-mbstring curl
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
sudo apt -yf install git unzip
