<?php
ini_set('default_charset', "UTF-8");
require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Adapter\Ftp;

const SESSION_ID_LENGTH = 32;
const INSTANCE_ID_LENGTH = 36;
const READ_QUEUE = 'persistent_storager';
const NOTIFICATION_BROKER_QUEUE = 'notifications_broker';

$ftp = new Ftp([
    'host' => '10.2.2.14',
    'username' => 'vagrant',
    'password' => 'vagrant',
    'root' => '/home/vagrant/src/public/mp3',
]);

$filesystem = new Filesystem($ftp);
$connection = new \PhpAmqpLib\Connection\AMQPStreamConnection('10.1.1.11', 5672, 'test', 'test');

$channel = $connection->channel();
$channel->queue_declare('persistent_storager', false, false, false, false);

$callback = function($msg) use ($filesystem, $channel){
  echo "  [x] Received new content\n";
  $filename = uniqid() . '.mp3';
  $instanceId = substr($msg->body, 0, INSTANCE_ID_LENGTH);
  $sessionId = substr($msg->body, INSTANCE_ID_LENGTH, SESSION_ID_LENGTH);
  $fileContent = substr($msg->body, INSTANCE_ID_LENGTH + SESSION_ID_LENGTH);
  $filesystem->write($filename, $fileContent);
  $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
  echo "  [x] Content saved, notifying public server\n\n";

  $message =  $instanceId . $sessionId . 'File has been uploaded to <a href="/mp3/' . $filename . '">' .$filename . '</a>';
  $channel->basic_publish(new AMQPMessage($message), '', NOTIFICATION_BROKER_QUEUE); // -- how to tag the consumer here?
  echo "\n  [x] Notification has been sent.\n";

};

$channel->basic_consume(READ_QUEUE, '', false, false, false, false, $callback);

echo "      I'm connected, waiting for new messages\n";

while(count($channel->callbacks)) {
    $channel->wait();
}
