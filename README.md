# To catch a rabbit
 RabbitMQ demo app, involving browser-based sound recording, and server-side wav->mp3 conversion and (maybe some day) Google Cloud upload.

 ![alt text](https://bigpanda.io/wp-content/uploads/2014/05/images_old-blog_2014_05_epicKid1.jpg "https://bigpanda.io/blog/vagrant-devbox/")

## Startup

Public_html server and mp3 compression and cloud upload workers require startup after vagrant up. To do so, SSH into server, and run startup script you'll find in src folder.

I decided no to run servers with PM2 or any other tool, because this repo is intented for demonstrating purposes, therefore constant re-starting, enabling and disabling tools like PM@ would be a nightmare. Also, adding arguments to node call (ie --inspect) would be problematic.

## Presentation

https://docs.google.com/presentation/d/e/2PACX-1vS2SzGq9WKFiuDbEI4-AtT2cVjGGINL7dczGSHIZ6dgRpiS9IX8W8MoccTMxWWTUDOaRnQ3ctj0Vv2C/pub?start=false&loop=false&delayms=60000

## Local places
 * http://10.1.1.11:15672/ - RabbitMQ admin console (test:test)
 * https://10.1.1.14:3001/ - app frontend

## Tools I use
* https://github.com/jankarres/node-lame - handles wav->mp3 compression, binding for native lame encoder
* https://github.com/zhuker/lamejs - handles wav->mp3 compression, emcsripten-compiled native code. Not used in this demo.
* https://github.com/thephpleague/flysystem - handles file storage
* https://developers.google.com/drive/v3/web/about-sdk
* https://github.com/nao-pon/flysystem-google-drive
* http://hungtran.co/long-polling-and-websockets-on-nodejs/ - I'm going to use it for notifications about job status
