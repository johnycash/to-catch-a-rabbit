//promise-style implementation

const amqp = require('amqplib'); // <-- check the diff - callback_api

var finalResult = {
  connection: null,
  channel: null,
  wav_2_mp3_encoder_queue: 'wav_2_mp3_encoder',
  notifications_queue: 'notifications',
  notifications_broker_queue: 'notifications_broker'
};

var promise = amqp.connect('amqp://test:test@10.1.1.11')
  .then(connection => {
    finalResult.connection = connection;
    return connection.createChannel();
  })
  .then(channel => {
    finalResult.channel = channel;
    //channel.prefetch(1);
    return channel.assertQueue(finalResult.wav_2_mp3_encoder_queue, {durable: false})
  })
  .then(result => {
    return finalResult.channel.assertQueue(finalResult.notifications_queue, {durable: false})
  })
  .then(result => {
    return finalResult.channel.assertQueue(finalResult.notifications_broker_queue, {durable: false})
  })
  .then(result => {
    return finalResult;
  });

//callback-style implementation
//
// const amqp = require('amqplib/callback_api');
//
// var promise = new Promise((resolve, reject) => {
//   amqp.connect('amqp://test:test@10.1.1.11', function(err, conn) {
//       if(err) {
//         reject(err);
//       }
//
//       conn.createChannel(function(err, ch) {
//         var q_write = 'wav_2_mp3_encoder';
//
//         ch.assertQueue(q_write, {durable: false});
//
//         resolve({
//           channel: ch,
//           wav_2_mp3_encoder_queue: q_write
//         });
//       });
//     });
// });

module.exports = promise;
