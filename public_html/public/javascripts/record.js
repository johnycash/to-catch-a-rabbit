var recordRTC = null;

var displayMessage = function(message) {
  if(!message) return;
  var $msgNode = $('#msgTemplate').clone();
  $msgNode
    .removeAttr('id')
    .append(message + '<br>')
    .show();

  $('#messages').append($msgNode);
}

var longPoll = function(){
  $.ajax({
    method: 'GET',
    url: '/messages',
    success: function(data){
      displayMessage(data);
    },
    complete: function(){
      longPoll()
    },
    timeout: 30000
  })
}


$(function() {
  if(navigator.mediaDevices !== undefined && navigator.mediaDevices.getUserMedia !== undefined) {
    $('#record').prop('disabled', false);

    navigator.mediaDevices.getUserMedia({audio: true, video: false})
    .then(function(mediaStream){
      recordRTC = RecordRTC(mediaStream, {
        recorderType: StereoAudioRecorder//, // optionally force WebAudio API to record audio
        //sampleRate: 11025 //nie odtwarza się prawidłowo
      });
    }, function() { console.log(arguments); alert('Old browser!'); })
    .catch(function() { console.log(arguments); alert('Old browser!'); });
  }

  $('body').on('click', '#record', function(){
    $(this).prop('disabled', true);
    $('#submit').prop('disabled', false);

    recordRTC.startRecording();
  })

  $('body').on('click', '#submit', function(){
    $(this).prop('disabled', true);

    recordRTC.stopRecording(function(audioURL) {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', audioURL, true);
      xhr.responseType = 'blob';
      xhr.onload = function(e) {
        if (this.status == 200) {

          var fd = new FormData();
          fd.append('record', this.response);

          $.ajax({
            type: 'POST',
            url: '/record',
            data: fd,
            processData: false,
            contentType: false
          })
          .done(function(data) {
            displayMessage(data);
          })
          .always(function(){
            $('#record').prop('disabled', false);
          });
        }
      };
      xhr.send();
    });
  })

  longPoll();
})
