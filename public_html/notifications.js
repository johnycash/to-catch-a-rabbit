const EventEmitter = require('events'),
  connectRabbit = require('./query.js'),
  instanceIdLength = 36,
  sessionIdLength = 32,
  notificationEmitterForInstance = {};

var sid = null;

/**
 * Message broking queue - PHP library is unable to send messages tagged with consumer tag,
 * so I use another queue to send everything, and then I use following worker
 * to pick messages from helper queue and repost them to the proper queue
 * tagged with consumer tag.
 */
connectRabbit
  .then(({channel, notifications_broker_queue, notifications_queue}) => {
    channel.consume(notifications_broker_queue, (msg) => {
      try {
        var instanceId = msg.content.slice(0, instanceIdLength).toString('utf8');
        var sessionId = msg.content.slice(instanceIdLength, instanceIdLength + sessionIdLength).toString('utf8');
        var body = msg.content.slice(instanceIdLength + sessionIdLength).toString('utf8');

        let message = {
          sessionId: sessionId,
          body: body
        };

        channel.sendToQueue(notifications_queue, new Buffer(JSON.stringify(message)), {consumerTag: instanceId});
        channel.ack(msg);
      } catch(e) {
        console.error(e);
      }
    }, {noAck: false})
  });

function getNotificationEmitterPromise(instanceId) {
  if(!notificationEmitterForInstance[instanceId]) {
    notificationEmitterForInstance[instanceId] = connectRabbit
    .then(({channel, notifications_queue}) => {
      var sessionNotificationsEmitter = new EventEmitter();

      channel.consume(notifications_queue, (msg) => {
        try {
          var message = JSON.parse(msg.content.toString('utf8'));
        } catch(e) {
          console.error(e);
          console.log('broken message received, skipping...')
          return;
        }

        try {
          console.log('Sending "' + message.body + '" to ' + message.sessionId + '...');
          sessionNotificationsEmitter.emit(message.sessionId, message.body);
          channel.ack(msg);
        } catch (e) {
          console.error(e);
          console.log('event emitter error, resheudling...')
          setTimeout(() => {
            try {
              console.log('Sending "' + message.body + '" to ' + message.sessionId + '...');
              sessionNotificationsEmitter.emit(message.sessionId, message.body);
              channel.ack(msg);
            } catch(e) {
              console.error(e);
              console.log('Re-emitting the message has failed, forgetting...');
              channel.ack(msg); //!
            }
          }, 3000);
        }

      }, {consumerTag: instanceId, noAck: false});

      return sessionNotificationsEmitter;
    });
  }

  return notificationEmitterForInstance[instanceId];
}

module.exports = getNotificationEmitterPromise;
