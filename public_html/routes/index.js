var uuid = require('uuid/v4');
var express = require('express');
var router = express.Router();
router.instanceId = uuid();
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var fs = require('fs');
const {resolve, basename} = require('path');
const connectRabbit = require('../query.js');
const notificationEmitterPromise = require('../notifications.js')(router.instanceId);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'To catch a rabbit' });
});

router.get('/record', function(req, res, next) {
  res.render('record', { title: 'Record something' });
});

router.post('/record', multipartMiddleware, function(req, res, next) {
  var newPath = resolve(__dirname + '/../uploads/' + basename(req.files.record.path) + '.wav');

  fs.promise.readFile(req.files.record.path)
    .then(data => {
      return connectRabbit.then(({channel, wav_2_mp3_encoder_queue}) => {
        console.log('Sending from ' + router.instanceId + ' for session ' + req.session.id);
        let messageBuffer = Buffer.concat([
          new Buffer.from(router.instanceId),
          new Buffer.from(req.session.id),
          new Buffer.from(data)
        ]);
        channel.sendToQueue(wav_2_mp3_encoder_queue, messageBuffer);
      });
    })
    .then(() => {
      res.send('Sheudled for compression and upload...');
    })
    .catch(error => {
      next(error);
    });
});

router.get('/messages', function(req, res, next) {
  notificationEmitterPromise.then(sessionNotificationsEmitter => {
    var autoClose = setTimeout(function(){
      sessionNotificationsEmitter.removeAllListeners(req.session.id);
      res.send('');
    }, 2000);

    var myListener = (message) => {
      clearTimeout(autoClose);
      sessionNotificationsEmitter.removeAllListeners(req.session.id); //!!!!
      console.log('got notification');
      res.send(message);
    }

    sessionNotificationsEmitter.on(req.session.id, myListener);
  })
})

router.get('/RecordRTC.min.js', function(req, res, next) {
  res.sendFile(resolve(__dirname + '/../node_modules/recordrtc/RecordRTC.min.js'));
});

console.log("\n\n My instance is: \n" + router.instanceId + " \n\n and I'm listening for notifications\n\n");

module.exports = router;

/**
 * Util
 */

fs.promise = {
  readFile: function(path) {
    return new Promise((resolve, reject) => {
      fs.readFile(path, (err, data) => {
        if(err) {
          reject(err);
        }

        resolve(data);
      })
    })
  }
};
