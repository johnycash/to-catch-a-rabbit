#!/bin/bash
cd /home/vagrant/src/certs
openssl req -new -x509 -newkey rsa:2048 -keyout keytmp.pem -out cert.pem -days 365 -nodes -subj "/C=PL/ST=LesserPoland/L=Krakow/O=BoldBrandCommerce/CN=10.1.1.14"
openssl rsa -in keytmp.pem -out key.pem
