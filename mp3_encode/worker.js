const amqp = require('amqplib'),
  Lame = require('node-lame').Lame,
  uuid = require('uuid/v4'),
  fs = require('fs'),
  q_listen = 'wav_2_mp3_encoder',
  q_write = 'persistent_storager',
  q_notifications = 'notifications',
  instanceIdLength = 36,
  sessionIdLength = 32;

var channel,
  consumer = function(msg) {
    var fid = uuid();

    //at the beginning we have id of sender instanceId and then sessionId
    //cut it out from the buffer for further usage
    var instanceId = msg.content.slice(0, instanceIdLength);
    var sessionId = msg.content.slice(instanceIdLength, instanceIdLength + sessionIdLength);
    var wavBuffer = msg.content.slice(instanceIdLength + sessionIdLength);

    var fStream = fs.createWriteStream(__dirname + '/uploads/' + fid + '.wav');
    console.log('  [x] I have a job from ' + instanceId.toString('utf8'));

    fStream.on('finish', () => {
      const encoder = new Lame({
          "output": "./uploads/" + fid + ".mp3",
          "bitrate": 192
      }).setFile("./uploads/" + fid + ".wav");

      encoder.encode()
        .then(() => {
          return fs.promise.readFile(__dirname + '/uploads/' + fid + '.mp3');
        })
        .then((buffer) => {
            console.log("  [x] Done, uploading content to the queue");
            let messageBuffer = Buffer.concat([
              instanceId,
              sessionId,
              buffer
            ]);
            channel.ack(msg);
            return channel.sendToQueue(q_write, messageBuffer);
        })
        .then(() => {
          console.log("  [x] Notifying html server ( id: " + instanceId + ' session: '+ sessionId + ")")

          let message = {
            sessionId: sessionId.toString('utf-8'),
            body: 'File has been compressed.'
          };

          return channel.sendToQueue(q_notifications, new Buffer(JSON.stringify(message)), {consumerTag: instanceId.toString('utf-8')});
        })
        .then(() => {
          console.log("  [x] Cleaning tmp files up")
          fs.unlink(__dirname + '/uploads/' + fid + '.mp3');
          fs.unlink(__dirname + '/uploads/' + fid + '.wav');
        });
    });

    fStream.write(wavBuffer);
    fStream.end();
  };

amqp.connect('amqp://test:test@10.1.1.11')
  .then(connection => {
    return connection.createChannel();
  })
  .then(myChannel => {
    channel = myChannel;
    return channel.assertQueue(q_listen, {durable: false});
  })
  .then(() => {
    return channel.assertQueue(q_write, {durable: false});
  })
  .then(() => {
    channel.consume(q_listen, consumer, {noAck: false});
    console.log("      Waiting for messages in %s. To exit press CTRL+C", q_listen);
  })
  .catch(e => console.error);

/**
 * Util
 */

fs.promise = {
  readFile: function(path) {
    return new Promise((resolve, reject) => {
      fs.readFile(path, (err, data) => {
        if(err) {
          reject(err);
        }
        resolve(data);
      })
    })
  }
};
